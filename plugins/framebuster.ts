import { Plugin } from '@nuxt/types'

const framebuster: Plugin = () => {
  if (window.self === window.top) {
    document.getElementsByTagName('body')[0].style.display = 'block'
  } else if (window.top) {
    window.top.location = window.self.location
  }
}

export default framebuster
