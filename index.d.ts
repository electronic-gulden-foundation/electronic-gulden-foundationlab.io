declare module '*.jpeg'
declare module '*.md'
declare module '*.png'

declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}
