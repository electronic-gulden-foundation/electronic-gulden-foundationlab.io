export const numberFormats = {
  nl: {
    'currency-eur': {
      style: 'currency',
      currency: 'EUR'
    },
    'currency-eur-no-fractions': {
      style: 'currency',
      currency: 'EUR',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    },
    'currency-eur-detailed': {
      style: 'currency',
      currency: 'EUR',
      minimumFractionDigits: 2,
      maximumFractionDigits: 3
    },
    'currency-btc': {
      style: 'currency',
      currency: 'BTC',
      currencyDisplay: 'symbol',
      minimumFractionDigits: 0,
      maximumFractionDigits: 8
    }
  }
}

export default numberFormats
