# Wat is de eGulden?

De eGulden is een cryptomunt. Dat betekent dat eGuldens schaars zijn. Net als de woodie die je hebt gevonden. Er
komen in totaal maximaal 21 miljoen eGuldens in omloop. Met deze woodie heb je er in ieder geval al een paar geclaimd.

De eGulden is opgericht in 2014 en bestaat dus al bijna net zo lang als Bitcoin. Bij de oprichting van de eGulden is
besloten dat Nederland en Nederlandstaligen de primaire doelgroep zijn. Daar horen de geocachers in Nederland
natuurlijk ook bij.
