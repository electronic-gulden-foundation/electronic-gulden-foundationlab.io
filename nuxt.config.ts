import { NuxtConfig } from '@nuxt/types'
import DynamicRoutes from './data/DynamicRoutes'
import { numberFormats } from './locales/numberFormats'

export default <NuxtConfig>{
  target: 'static',
  ssr: true,

  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/fontawesome'
  ],

  /**
   *  Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /**
   * Customize the generated output folder
   */
  generate: {
    dir: 'public',
    routes: DynamicRoutes
  },

  /**
   *  Nuxt.js modules
   */
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    '@nuxtjs/markdownit',
    '@nuxtjs/i18n',
    'vue-social-sharing/nuxt',
    '@nuxtjs/sitemap'
  ],

  /**
   *  Global CSS
   */
  css: [
    '@fortawesome/fontawesome-svg-core/styles.css',
    '~/assets/scss/common.scss'
  ],

  /**
   * Include resources in all styles, for global variables
   */
  styleResources: {
    scss: [
      '~/assets/scss/_variables.scss',
      '~/assets/scss/_mixins.scss',
      'bootstrap/scss/_functions.scss',
      'bootstrap/scss/_variables.scss',
      'bootstrap/scss/_mixins.scss'
    ],
    hoistUseStatements: true
  },

  /**
   *  Plugins to load before mounting the App
   */
  plugins: [
    {
      src: '~/plugins/framebuster.ts',
      mode: 'client'
    },
    {
      src: '~/plugins/vue-cookie-law.ts',
      mode: 'client'
    },
    {
      src: '~/plugins/vue-matomo.ts',
      mode: 'client'
    }
  ],

  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false
  },

  /**
   * Headers of the page
   */
  head: {
    title: 'De Nederlandse Cryptocurrency',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'De Nederlandse Cryptocurrency'
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }],
    script: [
      {
        src: 'https://egulden.server42.eu/ms.php?id=771&g=5',
        body: true,
        defer: true
      }
    ]
  },

  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: false,
    injected: true
  },

  build: {
    extractCSS: {
      ignoreOrder: true
    }
  },

  i18n: {
    defaultLocale: 'nl',
    lazy: true,
    langDir: 'locales',
    locales: [
      {
        code: 'nl',
        file: 'nl'
      }
    ],
    vueI18n: {
      numberFormats
    }
  },

  fontawesome: {
    component: 'fa',
    suffix: false
  },

  sitemap: {
    hostname: 'https://electronic-gulden-foundation.gitlab.io/'
  }
}
