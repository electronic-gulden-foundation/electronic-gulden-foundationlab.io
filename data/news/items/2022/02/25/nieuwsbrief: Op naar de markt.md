## Voorwoord

Een nieuwsbrief over eGulden is een nieuwsbrief over cryptogeld. Met de voorpagina willen we laten zien dat er nog veel te doen is, ondanks de voortgang. De reden dat Nederland ver achter loopt met cryptogeld adoptie heeft weinig te maken met technologie, maar met externe omstandigheden. Zo'n omstandigheid is NIRP (Negative Interest Rate Policy), negatieve rente op schuld. Zolang het hebben van schuld wordt beloond is het logisch dat er weinig aandacht is voor sparen en sparen is een van de belangrijkste peilers van cryptogeld.

Dat cryptogeld door het spaar-aspect bestemd is voor de conservatieve hoek is een zware misvatting. Iedere ondernemer die verder kijkt dan de korte termijn vindt een balans tussen zuinigheid en de noodzaak tot investeren. Tijd is de belangrijkste factor bij rendement en schuld trekt een zware wissel op duurzaamheid van een onderneming.

Maar, in navolging van Bitcoin-core, zijn ook wij conservatief en hollen wij niet achter alle verbetervoorstellen aan. Bij deze kondigen we de nieuwste versie van eGulden-core aan met versienummer 1.5. Het aantal wijzigingen is beperkt, maar qua functionaliteit en qua performance draait eGulden als een zonnetje en kan iedereen er met vol gewicht op leunen.  

In deze nieuwsbrief komen in volgorde aan bod:

- Een nieuwe corewallet, versie 1.5. 
- De online wallet efl.nl heeft een nieuw jasje en een reeks extra functies gekregen
- Een nieuwe eGulden marktplaats inclusief vouchers en communicatiemiddelen
- Epiloog; een verdieping

Als aanmoediging is een beloning ter waarde van 25 euro beschikbaar voor de honderd eerste authentieke deelnemers aan de marktplaats. Stuur een mailtje aan support@egulden.org voor de voorwaarden of lees in deze nieuwsbrief het onderdeel "een nieuwe marktplaats"

Wij wensen iedereen veel leesplezier en aarzel niet om ons een mailtje te sturen met vragen of suggesties, of desnoods om voor deze nieuwsbrief uit- of in te schrijven: support@egulden.org

Het vervolg van de nieuwsbrief is beschikbaar als 
- [PDF formaat](https://e-gulden.org/download/index.php?nieuwsbrief_2022-1)
- [Google doc](https://docs.google.com/document/d/1JcUf7N2QBdik6x1yMErzcAMOdEK5MVvxzmBGsG87CrI/edit?usp=sharing)

In- of uitschrijven op de nieuwsbrief: https://e-gulden.org/index.php/nieuwsbrieven
