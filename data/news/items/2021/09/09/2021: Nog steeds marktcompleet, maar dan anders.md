Vorig jaar was eGulden actief was op drie beurzen: Altilly, Folgory en Unnamed. Dit jaar zijn we dat nog steeds. Alleen 
zijn Altilly en Folgory afgevallen en zijn daar twee andere beurzen voor in de plaats gekomen.

### Wat is er gebeurd met Folgory?

De stichting wordt regelmatig benaderd door beurzen met een aanbieding om opgenomen te worden op een beurs tegen een 
zekere vergoeding. Zo kwam Folgory langs in 2020 op een moment dat we zo'n listing goed konden gebruiken. Met name 
omdat net Guldentrader moest afhaken vanwege de nieuwe registratieplicht bij de Nederlandse bank en de bijkomende 
kosten. Folgory was een jonge beurs, leek volop in ontwikkeling en kwam met een gunstig aanbod, maar bleek in tweede 
instantie erg onbetrouwbaar. Zij kwamen hun afspraken niet na, bleken slecht beveiligd en gaven zeer slechte 
ondersteuning. Daarom hebben wij besloten om Folgory zelf niet langer promoten. Het staat Folgory natuurlijk wel vrij 
om eGulden te blijven voeren, zoals iedere handelsbeurs dat kan.

### Wat is er gebeurd met Altilly?

Altilly is de beurs die na de teloorgang van Cryptopia tal van cryptomunten die op Cryptopia waren gelist heeft 
overgenomen. Helaas ondergingen ze na een succesvolle periode [hetzelfde lot als Cryptopia](https://www.qredit.io/altilly).

### Wat is de stand van unnamed?

[Unnamed] heeft enige tijd slecht gepresteerd, met name omdat withdrawels van diverse munten niet goed verliepen en 
heeft daardoor reputatieschade opgelopen. Met de handel en opname van EFL zijn nooit problemen geweest en naar het zich 
laat aanzien heeft unnamed de zaken weer op orde.

### Freiexchange een nieuwe naam voor de eGulden community!

[Freiexchange] is een handelsplaats die is opgericht door de oprichters van freicoin. Mede door de niet-commerciële 
opstelling van de oprichters heeft freicoin moeite gehad om een beurspostie te krijgen en te behouden en de stap is dan 
niet zo groot voor ontwikkelaars om zelf een beurs op te zetten. Freicoin bepaalt zelf welke munten zij wil opnemen 
zonder daar een listingfee voor te vragen. Met name door onze initiatieven in het kader van communitycoins.org kwam 
eGulden onder hun aandacht en zo zijn nu de belangrijkste samenwerkende communitycoins door freiexchange opgenomen.

### Last but not least: AtomicDex

[AtomicDex] is een experimentele decentrale beurs. Het orderboek van AtomicDex wordt onderhouden op een peer2peer netwerk 
(mm2), vergelijkbaar op de manier waarop core wallets met elkaar communiceren, maar dan voor het uitwisselen van 
marktorders. Als er een match wordt gevonden wordt deze door een ingenieuze reeks stappen via "atomic crosschain 
swaps" ingevuld. De voorwaarde voor het handelen is dat de computers van de koper en verkoper online moeten zijn, 
maar verder is er geen tussenpersoon. Het zijn puur de computers van de handelspartijen die de transactie veilig 
afhandelen. 

Een aardige bijkomstigheid is dat AtomicDex ook een HD-wallet is voor de deelnemende munten. Dit biedt dus een 
EFL-wallet waarbij je zelf de controle houdt over je private keys. De stichting heeft zelf ook een aantal servers 
opgezet om AtomicDex te ondersteunen. AtomicDex wordt ontwikkeld door het team van Komodo. Daar is de software 
ook te downloaden: 
[https://komodoplatform.com/en/wallets.html](https://komodoplatform.com/en/wallets.html)

Lees meer over beurzen op de wiki: [https://wiki.egulden.org/index.php?title=Beurzen](https://wiki.egulden.org/index.php?title=Beurzen)

[AtomicDex]: https://komodoplatform.com/en/wallets.html
[Freiexchange]: https://freiexchange.com/market/EFL/BTC 
[Unnamed]: https://www.unnamed.exchange/Exchange?market=EFL_BTC
