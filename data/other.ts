export default [
  {
    name: 'De oude website',
    href: 'https://egulden.org/index.php/de-egulden'
  },
  {
    name: 'EFL vriendencampagne',
    href: 'https://egulden.org/vrienden.php'
  },
  {
    name: 'Zoeken...',
    href: '/zoeken'
  },
  {
    name: 'WIKI',
    href: 'https://wiki.egulden.org'
  },
  {
    name: 'Nieuwsbrief',
    href: 'https://e-gulden.org/index.php/nieuwsbrieven'
  },
  {
    name: 'Broncode',
    href: 'https://gitlab.com/electronic-gulden-foundation'
  }
]
