export default [
  {
    name: 'AtomicDex',
    href: 'https://atomicdex.io/',
    image: require('./img/atomicdex.png')
  },
  {
    name: 'FreiExchange',
    href: 'https://freiexchange.com/market/EFL/BTC',
    image: require('./img/freiexchange.png')
  },
  {
    name: 'Xeggex',
    href: 'https://xeggex.com/market/EFL_BTC?ref=6554ec450d32f604e15786f8',
    image: require('./img/logo-200.webp')
  },
  {
    name: 'Mubadil',
    href: 'https://mubadil.com/en/?ref=95',
    image: require('./img/mubadil3.png')
  },
  {
    name: 'NOHODL-markt',
    href: 'https://nohodl.org',
    image: require('./img/nohodl2.png')
  }
  
]
