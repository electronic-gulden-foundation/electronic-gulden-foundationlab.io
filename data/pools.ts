export default [
  {
    name: 'pool.amazingsystems.nl',
    href: 'https://pool.amazingsystems.nl',
    certified: true
  },
  {
    name: 'Stichting eFiliaal',
    href: 'http://pool.efiliaal.nl',
    certified: true
  },
  {
    name: 'Redpool',
    href: 'https://redpool.pl/',
    certified: false
  },
  {
    name: 'A Mining Pool',
    href: 'https://aminingpool.com/',
    certified: false
  }
]
